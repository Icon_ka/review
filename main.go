package main

import (
    "log"
    "time"
    "net/http"
    "os"
    "review/internal/router"
    "os/signal"
    "syscall"
    "context"
    "review/internal/controller"
)

func main() {

    ctrl := controller.NewControllers()
    r := router.NewApiRouter(ctrl)

    // Создание HTTP-сервера
    server := &http.Server{
        Addr:         ":8080",
        Handler:      r,
        ReadTimeout:  10 * time.Second,
        WriteTimeout: 10 * time.Second,
    }

    // Запуск сервера в отдельной горутине
    go func() {
        log.Println("Starting server...")
        if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
            log.Fatalf("Server error: %v", err)
        }
    }()

    // Создание канала для получения сигналов остановки
    sigChan := make(chan os.Signal, 1)
    signal.Notify(sigChan, syscall.SIGINT)

    // Ожидание сигнала остановки
    <-sigChan

    // Создание контекста с таймаутом для graceful shutdown
    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    // Остановка сервера с использованием graceful shutdown
    err := server.Shutdown(ctx)
    if err != nil {
        log.Fatalf("Server shutdown error: %v", err)
    }

    // Ожидание завершения работы сервера
    <-ctx.Done()

    log.Println("Server stopped gracefully")
}
