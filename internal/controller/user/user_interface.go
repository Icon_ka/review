package user

import (
    "net/http"
)

type Userer interface {
    Get(http.ResponseWriter, *http.Request)
}
