package user

import (
    "review/internal/service/user"
    "net/http"
    "encoding/json"
)

type UserController struct {
    service user.UserServicer
}

func NewUserController() *UserController {
    return &UserController{
        service: user.NewUserService(),
    }
}

func (u *UserController) Get(writer http.ResponseWriter, request *http.Request) {
    resp, err := u.service.GetUser()
    if err != nil {
        http.Error(writer, "Error: "+err.Error(), http.StatusInternalServerError)
        return
    }

    jsonData, err := json.Marshal(resp)
    if err != nil {
        http.Error(writer, "Error: "+err.Error(), http.StatusInternalServerError)
        return
    }

    writer.Header().Set("Content-Type", "application/json")
    writer.Write(jsonData)
}
