package controller

import "review/internal/controller/user"

type Controllers struct {
    User user.Userer
}

func NewControllers() *Controllers {
    return &Controllers{
        User: user.NewUserController(),
    }
}
