package entites

import "encoding/json"

func (r Record) MarshalJSON() ([]byte, error) {
    type Alias Record
    if r.Amount > 50000 {
        r.Email = ""
        r.Username = ""
        r.Profile.FirstName = ""
        r.Profile.LastName = ""
        r.Profile.Avatar = ""
    }
    return json.Marshal((Alias)(r))
}

type Response struct {
    Records     []Record `json:"records"`
    Skip        int64    `json:"skip"`
    Limit       int64    `json:"limit"`
    TotalAmount int64    `json:"totalAmount"`
}

type Record struct {
    ID        int64   `json:"id" db:"id"`
    Email     string  `json:"email" db:"email"`
    Amount    int     `json:"amount" db:"amount"`
    Profile   Profile `json:"profile" db:"profile_id"`
    Password  string  `json:"-" db:"password"`
    Username  string  `json:"username" db:"username"`
    CreatedAt string  `json:"createdAt" db:"created_at"`
    CreatedBy string  `json:"createdBy" db:"created_by"`
}

type Profile struct {
    Dob        string `json:"dob" db:"dob"`
    Avatar     string `json:"avatar" db:"avatar"`
    LastName   string `json:"lastName" db:"last_name"`
    FirstName  string `json:"firstName" db:"first_name"`
    StaticData string `json:"-" db:"StaticData"`
}
