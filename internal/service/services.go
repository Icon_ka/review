package service

import (
    "review/internal/service/user"
)

type Services struct {
    User user.UserServicer
}

func NewControllers() *Services {
    return &Services{
        User: user.NewUserService(),
    }
}
