package user

import (
    "review/internal/demoClient"
    "review/internal/entites"
    "math/rand"
)

type UserService struct {
    demo demoClient.Demorer
}

func NewUserService() *UserService {
    return &UserService{demo: demoClient.NewDemo()}
}

func (u *UserService) GetUser() (entites.Response, error) {
    resp, err := u.demo.Get()
    if err != nil {
        return entites.Response{}, err
    }

    for i, _ := range resp.Records {
        resp.Records[i].Amount += rand.Intn(100000)
    }

    return resp, nil
}
