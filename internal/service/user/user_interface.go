package user

import "review/internal/entites"

type UserServicer interface {
    GetUser() (entites.Response, error)
}
