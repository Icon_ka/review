package demoClient

import (
    "net/http"
    "encoding/json"
    "review/internal/entites"
    "crypto/tls"
)

type Demorer interface {
    Get() (entites.Response, error)
}

type Demo struct {
    client *http.Client
    url    string
}

func NewDemo() *Demo {
    tr := &http.Transport{
        TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
    }
    client := &http.Client{Transport: tr}

    demo := &Demo{
        client: client,
        url:    "https://demo.apistubs.io/api/v1/users",
    }
    return demo
}

func (d *Demo) Get() (entites.Response, error) {
    req, err := http.NewRequest("GET", d.url, nil)
    if err != nil {
        return entites.Response{}, err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Connection", "keep-alive")

    resp, err := d.client.Do(req)
    if err != nil {
        return entites.Response{}, err
    }
    defer resp.Body.Close()

    var response entites.Response
    err = json.NewDecoder(resp.Body).Decode(&response)
    if err != nil {
        return entites.Response{}, err
    }

    return response, nil
}
