package router

import (
    "github.com/go-chi/chi"
    "net/http"
    "review/internal/controller"
)

func NewApiRouter(controllers *controller.Controllers) http.Handler {
    r := chi.NewRouter()

    r.Get("/api/users", controllers.User.Get)

    return r
}
